create table if not exists holidays
(
    id          bigint auto_increment primary key,
    date_of     date not null,
    description varchar
);

create unique index if not exists holidays_date_uindex
    on holidays (DATE_OF desc);

comment on table holidays is 'Праздничные дни (включая те, которые обычно являются рабочими, но из-за того, что праздник выпал на выходной, их сделали выходными) в соответствии с НПА РК. Не содержит суббот и воскресений';

create table if not exists workingdays
(
    id          bigint auto_increment primary key,
    date_of     date not null,
    description varchar
);

create unique index if not exists workdays_date_uindex
    on workingdays (DATE_OF desc);

comment on table workingdays is 'Дни, которые обычно являются выходними, но в связи с праздниками их сделали рабочими в соответствии с НПА РК';
