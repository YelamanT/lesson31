package kz.kaspi.lesson31.service;


import kz.kaspi.lesson31.dto.Day;
import kz.kaspi.lesson31.dto.PeriodInfoDTO;
import kz.kaspi.lesson31.dto.PeriodInfoRequest;
import kz.kaspi.lesson31.entity.WorkingDay;
import kz.kaspi.lesson31.entity.Holiday;
import kz.kaspi.lesson31.repository.HolidayRepository;
import kz.kaspi.lesson31.repository.WorkdayRepository;
import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.DAYS;

@Service
public class MainService {

    private final HolidayRepository holidayRepository;

    private final WorkdayRepository workdayRepository;

    public MainService(HolidayRepository holidayRepository, WorkdayRepository workdayRepository) {
        this.holidayRepository = holidayRepository;
        this.workdayRepository = workdayRepository;
    }


    public PeriodInfoDTO holidaysInPeriod(PeriodInfoRequest request) {

        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);



        long a = DAYS.between(dateFrom, dateTo);

        long b = getWeekends(dateFrom, dateTo);
        long c = getHolidays(dateFrom, dateTo);
        long d = getWorkdays(dateFrom, dateTo);

        long holidays = b + c - d;
        long workdays = a - holidays;

        return new PeriodInfoDTO(workdays, holidays);
    }

    private long getWorkdays(LocalDate dateFrom, LocalDate dateTo) {
        return workdayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getHolidays(LocalDate dateFrom, LocalDate dateTo) {
        return holidayRepository.findAllByDateGreaterThanEqualAndDateLessThanEqual(dateFrom, dateTo)
                .size();
    }

    private long getWeekends(LocalDate dateFrom, LocalDate dateTo) {
        return dateFrom.datesUntil(dateTo)
                .filter(it -> isWeekend(it))
                .count();
    }

    public List<Day> listInPeriod(PeriodInfoRequest request) {

        LocalDate dateFrom = request.getDateFrom();
        LocalDate dateTo = request.getDateTo().plusDays(1);

        List<WorkingDay> workDayList = workdayRepository.findAll();
        List<Holiday> holidayList = holidayRepository.findAll();
        List<Day> days = dateFrom.datesUntil(dateTo)
                .map(localDate -> {


                    boolean isHoliday = (
                            isWeekend(localDate)
                                    && isNotWorkingDay(localDate, workDayList)
                    ) ||
                            isHoliday(localDate, holidayList);


                    return new Day(localDate, isHoliday);
                }).collect(Collectors.toList());

        return days;
    }


    private boolean isWeekend(LocalDate date) {
        return date.getDayOfWeek() == DayOfWeek.SATURDAY ||
                date.getDayOfWeek() == DayOfWeek.SUNDAY;
    }


    private boolean isHoliday(LocalDate date, List<Holiday> holidayList) {
        return holidayList.stream()
                .anyMatch(it -> it.getDate().isEqual(date));
    }

    private boolean isNotWorkingDay(LocalDate date, List<WorkingDay> workingDayList) {
        return workingDayList.stream()
                .noneMatch(it -> it.getDate().isEqual(date));
    }

}
