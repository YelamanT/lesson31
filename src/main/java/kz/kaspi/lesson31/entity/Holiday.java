package kz.kaspi.lesson31.entity;


import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "holidays")
public class Holiday {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date_of")
    private LocalDate date;

    private String description;


    public Holiday() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
