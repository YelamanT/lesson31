package kz.kaspi.lesson31.dto;

import java.time.LocalDate;


public class Day {

    private LocalDate date;
    private boolean dayOff;


    public Day(LocalDate date, boolean isHoliday) {
        this.date = date;
        this.dayOff = isHoliday;
    }


    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isHoliday() {
        return dayOff;
    }

    public void setHoliday(boolean holiday) {
        dayOff = holiday;
    }
}
