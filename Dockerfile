FROM gradle:jdk11 as builder
COPY --chown=gradle:gradle . .
RUN gradle build

FROM openjdk:11.0.10-jre-slim-buster
RUN addgroup appuser && adduser --disabled-password --ingroup appuser appuser
WORKDIR /app
RUN chown appuser:appuser /app
USER appuser
COPY --from=builder --chown=appuser:appuser /home/gradle/build/libs/*.jar /app/state-holidays.jar
EXPOSE 8080
ENTRYPOINT exec java -jar state-holidays.jar --spring.profiles.active=\${ACTIVE_PROFILE}